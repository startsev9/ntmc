﻿#include <iostream>

using namespace std;

int main()
{
    setlocale(LC_ALL, "Russian");

    int n, d, r1, r2, q, r;
    cout << "Введите n:    ";
    cin >> n;
    d = 2 * floor(pow(n, 1 / 3.) + 0.5) + 1;

    r1 = floor(fmod(n, d) + 0.5);
    r2 = floor(fmod(n, d - 2) + 0.5);
    q = 4 * (n / (d - 2) - n / d);
    int s = sqrt(n);

    while (true) {
        d = d + 2;
        if (d > s) {
            cout << "Делителя нет" << endl;
            return 0;
        }
        
        r = 2 * r1 - r2 + q;
        
        if (r < 0) {
            r = r + d;
            q = q + 4;
        }

        while (r >= d) {
            r = r - d;
            q = q - 4;
        }

        if (r == 0) {
            cout << "Ответ: " << d << endl;
            return 0;
        }
        else {
            r2 = r1;
            r1 = r;
        }
        {

        }
    }

    return 0;
}