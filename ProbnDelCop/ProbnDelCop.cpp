﻿#include <iostream>

void print(int *mas, int t)
{
    for (int i = 0; i < t; i++)
    {
        std::cout << mas[i] << "\t";
    }
}

int NextDivider(unsigned int d)
{
    while (1)
    {
        int c = 1;
        d++;
        for (int i = 2; i <= d; i++)
            if (d % i == 0)
                c++;
        if (c == 2)
            return d;
    }
}

int main()
{
    setlocale(LC_ALL, "Russian");

    unsigned int n, b;
    std::cout << "Введите n:    ";
    std::cin >> n;
    std::cout << "Введите границу B:    ";
    std::cin >> b;
    int r, q, t = 0;
    bool m = true;
    int d = 2;
    int* p = new int[30]();

    if (n == 1) {
        std::cout << "\nВведите другое n";
        delete[] p;
        return 0;
    }

    while (n != 1) {

        r = n % d;
        q = n / d;

        if (r == 0) {
            p[t] = d;
            t++;
            n = q;
        }
        else if (q > d) {
            d = NextDivider(d);
            if (d > b) {
                m = false;
                break;
            }
        }
        else {
            p[t] = n;
            t++;
            break;
        }
    }

    if (m)
        std::cout << "Факторизация полная" << std::endl;
    else
        std::cout << "Факторизация неполная, остаток: " << n << std::endl;

    print(p, t);
    delete[] p;
    return 0;
}