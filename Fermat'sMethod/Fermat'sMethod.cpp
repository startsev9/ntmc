﻿#include <iostream>

using namespace std;

bool isSquareRootExists(int n, int p)
{
    n = n % p;

    for (int x = 2; x < p; x++)
        if ((x * x) % p == n)
            return true;
    return false;
}

void FermatFactors(int n) 
{
    int y, z = 0; bool f = true;
    int x = ceil(sqrt(n));

    if (x * x == n) {
        cout << "a = b = x = " << x << endl;
        f = false;
    }

    while (f == true) {
        x++;
        if (x > (n + 9) / 6) {
            cout << "n - простое" << endl;
            break;
        }

        z = x * x - n;
        y = ceil(sqrt(z));
        if (y * y == z) {
            cout << "Ответ: a = " << x + y << ", b = " << x - y << endl;
            break;
        }  
    }
}

int FermatWithSifting(int n) {
    int x = ceil(sqrt(n));
    int m[2], r[2];
    int i, j, z, y;

    if (x * x == n) {
        cout << "a = b = x = " << x << endl;
        return 0;
    }

    for (i = 0; i < 3; i++) {
        cout << "\nВведите " << i << " модуль: ";
        cin >> m[i];
    }

    int** s = new int* [3];
    for (i = 0; i < 3; i++)
        s[i] = new int[5];

    for (i = 0; i < 3; i++)
        for (j = 0; j < 5; j++)
            s[i][j] = -1;

    for (i = 0; i < 3; i++)
        for (j = 0; j < 5; j++)
            if (isSquareRootExists(j * j - n, m[i]) || (j * j == int(fmod(0, m[i]))))
                s[i][j] = 1;
            else
                s[i][j] = 0;
    
    x++;
    for (i = 0; i < 3; i++)
        r[i] = fmod(x, m[i]);
    while (true) {
        for (i = 0; i < 3; i++)
            if (s[i][r[i]] == 1) {
                z = x * x - n;
                y = ceil(sqrt(z));
            }

        if (y * y == z) {
            cout << "Ответ: a = " << x + y << ", b = " << x - y << endl;
            return 0;
        }

    }
}


int main()
{
    setlocale(LC_ALL, "Russian");

    int n, f;
    cout << "Введите нечетное n: ";
    cin >> n;
    cout << "\nКакой алгоритм Ферма вы выберете?\n0 - обычный, 1 - с просеиванием ";
    cin >> f;

    if ((n <= 0) || ((n & 1) == 0))
    {
        cout << "Неверные вводные данные";
        return 0;
    }
    else if (f == 0) {
        FermatFactors(n);
    }
    else if (f == 1) {
        FermatWithSifting(n);
    }
    return 0;
}